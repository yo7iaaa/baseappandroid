package com.dev.baseapp.others.helpers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Amit11 on 11/20/2016.
 */
public class ErrorHandler {

    public String code;
    public String setError(String code){
        String Message = null;
        switch (code){
            case "2100":
                Message = "required/format fields fields needed";
                return Message;
            case "2101":
                Message = "User exist with this username";
                return Message;

            case "2102":
                Message = "User exist with this email";
                return Message;

            case "2103":
                Message = "Invalid username";
                return Message;

            case "2104":
                Message = "Invalid password ";
                return Message;

            case "2105":
                Message = "User not activated";
                return Message;

            case "2106":
                Message = " Account with this email is not exist";
                return Message;

            case "2107":
                Message = "Account with this username/email is not exist";
                return Message;

            case "2108":
                Message = "Invalid old password";
                return Message;

            case "2109":
                Message = "User exist with this telephone";
                return Message;

            case "2110":
                Message = " empty cart";
                return Message;

            case "2112":
                Message = "invalid username or email";
                return Message;

            case "2222":
                Message = "New version of Offary available for download , kindly update";
                return Message;
            default:
                return  "";

        }

    }
    public String setErrorValidation(Object code){

        try {
            JSONObject jsonObject = new JSONObject(code.toString());
            Iterator<?> keys = jsonObject.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                if ( jsonObject.get(key) instanceof JSONArray) {
                    return jsonObject.getJSONArray(key).get(0).toString();
                }
            }        } catch (JSONException e) {
            e.printStackTrace();
        }
    return "UnKnown Error";
    }
}

