package com.dev.baseapp.others.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerDialogPrev extends DialogFragment {
public static final String ARG_DEFAULT = "default";

private DatePicker datePicker;
private String defaultDate;
    OnDatePickerDoneButtonClickedListener listener;
    public DatePickerDialogPrev()
    {

    }
    public void setDatePickerDialog(OnDatePickerDoneButtonClickedListener listener)
    {
        this.listener = listener;
    }

public interface OnDatePickerDoneButtonClickedListener {
    void OnDatePickerDoneButtonClicked(String date);
}

@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//    defaultDate = getArguments().getString(ARG_DEFAULT);
}

@Override
public Dialog onCreateDialog(Bundle savedInstanceState) {
    Calendar calendar = Calendar.getInstance();
    if (TextUtils.isEmpty(defaultDate)) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            calendar.setTime(df.parse(new Date().toString()));
            Log.d("first", defaultDate + " -");
        } catch (ParseException e) {
            e.printStackTrace();
        }

    } else {
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            calendar.setTime(df.parse(defaultDate));
            Log.d("second", defaultDate + " -");
        } catch (ParseException e1) {
            Log.d("third",defaultDate+" -");
        }
    }
    datePicker = new DatePicker(getActivity());

    datePicker.setCalendarViewShown(false);
    datePicker.setMaxDate(new Date().getTime());
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {

            String date = datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear();
//            listener.OnDatePickerDoneButtonClicked(date);
            final Calendar calendar = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            listener.OnDatePickerDoneButtonClicked(df.format(calendar.getTime()));

            dismiss();
        }
    })
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dismiss();
                }
            })
                    //.setTitle("Select date")
            .setView(datePicker);
    // Create the AlertDialog object and return it
    return builder.create();
}
}
