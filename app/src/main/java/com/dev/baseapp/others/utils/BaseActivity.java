package com.dev.baseapp.others.utils;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;

import java.util.Locale;


public class BaseActivity extends AppCompatActivity {


    public void CheckLanguage() {
        Locale locale = new Locale(PrefManager.isLangArabic(this) ? "ar" : "en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

    }


}
