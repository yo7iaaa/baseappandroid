package com.dev.baseapp.others.utils;

public class Constants {
    public static final String PREF_NAME = "Secure";
    public static final String PREF_LOGGED_IN = "logedin";
    public static final String PREF_USER = "user";
    public static final String PREF_GUEST = "PREF_GUEST";
    public static final String PREF_GUESTID = "PREF_GUESTID";
    public static final String PREF_USER_ACTION = "useraction";
    public static final String PREF_Country = "user";
    public static final int REQUEST_CAMERA = 1;
    public static final int SELECT_FILE = 2;

    public static String APIBaseURL = "http://5.189.184.254:7773/api/v2/";
    public static String APIBaseURLWithoutV2 = "http://5.189.184.254:7773/";
    public static String GetItemsInCategory = APIBaseURL + "products?category_id=";
    public static String GetBranches = APIBaseURL + "common/branches2";
    public static String GetCategory = APIBaseURL + "categories";
    public static String GetOffers = APIBaseURL + "Common/GetDiscountsImages";
    public static String token = APIBaseURLWithoutV2 + "token";
    public static String userInfo = APIBaseURL + "api/Users/GetCurrentUserInfo";
    public static String Register = APIBaseURL + "api/Users/Register";
    public static String UpdateProfile = APIBaseURL + "api/Users/UpdateProfile";
}