package com.dev.baseapp.others.Networking;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dev.baseapp.others.Models.UserDTO;
import com.dev.baseapp.others.helpers.ResultHandler;
import com.dev.baseapp.others.utils.Constants;
import com.dev.baseapp.others.utils.PrefManager;
import com.dev.baseapp.others.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import offary.amitsoftware.com.sweetalert.SweetAlertDialogProgress;


public class NetworkManager {

    Activity context;
    ProgressDialog _ProgressDialog;
    private MyCustomObjectListener listener;

    public NetworkManager(Activity context) {
        this.context = context;
    }

    public void setCustomObjectListener(MyCustomObjectListener listener) {
        this.listener = listener;
    }

    public interface MyCustomObjectListener {

        void onObjectReady(JSONObject title, String Key);

        void onFailed(String title);


    }


    public void callServerPost(Activity activity,String Url, JSONObject parameters, HashMap<String, String> Header, final JSONObjectRequestListener responseListener) {
        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.setCancelable(false);
        sweetAlertDialogProgress.show();
        Log.d("testUrl",Url+parameters);
        String lang = PrefManager.isLangArabic(activity) ? "2" : "1";
        Header.put("language_Id", lang);
        Header.put("languageId", lang);
        String par = (Url.contains("?") ? "&" : "?" )+"languageId="+lang+"&language_Id="+lang;
        if (PrefManager.isLoggedUser(activity))
            Header.put("Authorization", UserDTO.getInstance().getToken());

        AndroidNetworking.post(Url+par)
                .addJSONObjectBody(parameters)
                .setTag(Url)
                .addHeaders(Header)
                .setContentType("application/json")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("apidata", response.toString());
                        sweetAlertDialogProgress.dismiss();


                        if (new ResultHandler().validateHandlerResult(context, response)) {
                            responseListener.onResponse(response);
                        }
                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        sweetAlertDialogProgress.dismiss();
                        Utils.NoInternetConnection(context);
                        responseListener.onError(error);


                        // handle error
                        Log.d("apidata", error.getErrorBody().toString());
                    }
                });
    }

    public void callServerPostString(Activity activity,String Url, String parameters, HashMap<String, String> Header, final JSONObjectRequestListener responseListener) {
        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.setCancelable(false);
        sweetAlertDialogProgress.show();
        Log.d("testUrl",Url+parameters);
        String lang = PrefManager.isLangArabic(activity) ? "2" : "1";
        Header.put("language_Id", lang);
        Header.put("languageId", lang);
        String par = (parameters.contains("?") ? "&" : "?" )+"languageId="+lang+"&language_Id="+lang;
        if (PrefManager.isLoggedUser(activity))
            Header.put("Authorization", UserDTO.getInstance().getToken());

        AndroidNetworking.post(Url)
                .addStringBody(parameters+par)
                .setTag(Url)
                .addHeaders(Header)
                .setContentType("application/json")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("apidata", response.toString());
                        sweetAlertDialogProgress.dismiss();

                        if (new ResultHandler().validateHandlerResult(context, response)) {
                            responseListener.onResponse(response);
                        }
                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        sweetAlertDialogProgress.dismiss();

                        Utils.NoInternetConnection(context);
                        responseListener.onError(error);


                        // handle error
                        Log.d("apidata", error.getErrorBody().toString());
                    }
                });
    }

    public void callServerGet(Activity activity,String Url, String parameters, HashMap<String, String> Header, final JSONObjectRequestListener responseListener) {
        final SweetAlertDialogProgress sweetAlertDialogProgress = new SweetAlertDialogProgress(context);
        sweetAlertDialogProgress.setCancelable(false);
        sweetAlertDialogProgress.show();
        Log.d("testUrl",Url+parameters);
        String lang = PrefManager.isLangArabic(activity) ? "2" : "1";
        Header.put("language_Id", lang);
        Header.put("languageId", lang);
        String par = ((Url+parameters).contains("?") ? "&" : "?" )+"languageId="+lang+"&language_Id="+lang;
        Log.d("testUrl",Url+parameters+par);
        if (PrefManager.isLoggedUser(activity))
            Header.put("Authorization", UserDTO.getInstance().getToken());
        AndroidNetworking.get(Url + parameters + par)
                .setTag(Url)
                .addHeaders(Header)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("apidata", response.toString());
                        sweetAlertDialogProgress.dismiss();

                        if (new ResultHandler().validateHandlerResult(context, response)) {
                            responseListener.onResponse(response);

                        }

                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        sweetAlertDialogProgress.dismiss();
//                        Utils.NoInternetConnection(context);
                        Log.d("apidataError", error.getErrorBody().toString());

                        try {
                            new ResultHandler().validateHandlerResult(context, new JSONObject(error.getErrorBody()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        responseListener.onError(error);

                        // handle error
                    }
                });
    }


    public void TestNetwork(Activity activity,String phone, String password) {
        String parameters = "";

        callServerPostString(activity,"URL", parameters, new HashMap<String, String>(), new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                if (listener != null) {
                    listener.onObjectReady(response, "TestNetwork");
                }
            }

            @Override
            public void onError(ANError anError) {
                if (listener != null) {
                    listener.onFailed(anError.getErrorBody().toString());
                }
            }
        });
    }



}
