package com.dev.baseapp.others.utils;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.dev.baseapp.R;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import offary.amitsoftware.com.sweetalert.SweetAlertDialog;
import offary.amitsoftware.com.sweetalert.SweetAlertDialogFailed;


/**
 * Created by Android Developer on 2/24/2016.
 */
public class Utils {

    private static NotificationCompat.Builder mBuilder;
    private static NotificationManager mNotifyManager;
    private static int id = 1;
    private static int counter = 0;
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 2);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static int DifferenceBetweenDates(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);
        return Math.round(elapsedDays);

    }
    public static <T> List<T> toList(String json, final TypeToken clazz) {
        if (null == json) {
            return null;
        }
        String yourJson = json;
        Type listType = clazz.getType();
        return new Gson().fromJson(yourJson, listType);
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }

    public static void ShowErrorDialog(Activity activity, String title) {
        SweetAlertDialogFailed sweetAlertDialog = new SweetAlertDialogFailed(activity, SweetAlertDialogFailed.WARNING_TYPE);
        sweetAlertDialog.setTitle("Error");
        sweetAlertDialog.setConfirmText("Dismiss");

        sweetAlertDialog.setContentText(title);
        sweetAlertDialog.show();
    }

    public static int getScreenWidth(Context c) {
        // if (screenWidth == 0) {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        //}

        return screenWidth;
    }

    public static String createDate(long timestamp) {
        Log.d("BirthDate", timestamp + "-");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp * 1000);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        return sdf.format(d);
    }

    public static Date createDatelong(long timestamp) {
        Log.d("BirthDate", timestamp + "-");
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp * 1000);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        return (d);
    }

    public static Drawable GetVectorWithColor(Activity activity, int Color, int Drawable) {
        final android.graphics.drawable.Drawable drawable = DrawableHelper
                .withContext(activity)
                .withColor(Color)
                .withDrawable(Drawable)
                .tint()
                .get();
        return drawable;
    }

    public static AnimationSet animateHeartButton() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 360);
        rotateAnimation.setDuration(300);
        rotateAnimation.setFillAfter(true);
        rotateAnimation.setInterpolator(new AccelerateInterpolator());

        ScaleAnimation scaleAnimation = new ScaleAnimation(0.2f, 1f, 0.2f, 1f);
        scaleAnimation.setDuration(300);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setInterpolator(new OvershootInterpolator());

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.setFillAfter(true);
        animationSet.setDuration(300);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);
        return animationSet;
    }

    public static Drawable ChangeDrawableColor(Context context, int drawable, int color) {
        Drawable mDrawable = context.getResources().getDrawable(drawable);
        mDrawable.setColorFilter(new
                PorterDuffColorFilter(context.getResources().getColor(color), PorterDuff.Mode.SRC_ATOP));
        return mDrawable;
    }

    public static Drawable ChangeDrawableColor(Context context, int drawable, String color) {
        Drawable mDrawable = context.getResources().getDrawable(drawable);
        mDrawable.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP));
        return mDrawable;
    }

    public static void ChangeRatingStarsColor2(RatingBar ratingBar, String Color, Boolean ChangeAllCheck) {
        // Call some material design APIs here
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        if (ChangeAllCheck) {
            stars.getDrawable(0).setColorFilter(android.graphics.Color.parseColor(Color), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(android.graphics.Color.parseColor(Color), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(2).setColorFilter(android.graphics.Color.parseColor(Color), PorterDuff.Mode.SRC_ATOP);
        } else
            stars.getDrawable(2).setColorFilter(android.graphics.Color.parseColor(Color), PorterDuff.Mode.SRC_ATOP);


    }

    //    public static String getTimeAgo(long time) {
//         final List<Long> times = Arrays.asList(
//                TimeUnit.DAYS.toMillis(365),
//                TimeUnit.DAYS.toMillis(30),
//                TimeUnit.DAYS.toMillis(1),
//                TimeUnit.HOURS.toMillis(1),
//                TimeUnit.MINUTES.toMillis(1),
//                TimeUnit.SECONDS.toMillis(1) );
//         final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");
//          final int SECOND_MILLIS = 1000;
//          final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
//          final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
//          final int DAY_MILLIS = 24 * HOUR_MILLIS;
//        if (time < 1000000000000L) {
//            // if timestamp given in seconds, convert to millis
//            time *= 1000;
//        }
//
//        long now = new Date().getTime();
//        if (time > now || time <= 0) {
//            return "just now";
//        }
//
//        // TODO: localize
//        final long diff = now - time;
//        if (diff < MINUTE_MILLIS) {
//            return "الأن";
//        } else if (diff < 2 * MINUTE_MILLIS) {
//            return "منذ دقيقة";
//        } else if (diff < 50 * MINUTE_MILLIS) {
//            return "منذ "+diff / MINUTE_MILLIS + "دقيقة";
//        } else if (diff < 90 * MINUTE_MILLIS) {
//            return "منذ ساعة";
//        } else if (diff < 24 * HOUR_MILLIS) {
//            return "منذ "+diff / HOUR_MILLIS + "ساعات";
//        } else if (diff < 48 * HOUR_MILLIS) {
//            return "بالأمس";
//        } else {
//            return "منذ "+diff / DAY_MILLIS + " أيام";
//        }
//    }
    public static String getStringSizeLengthFile(long size) {

        DecimalFormat df = new DecimalFormat("0.00");

        float sizeKb = 1024.0f;
        float sizeMo = sizeKb * sizeKb;
        float sizeGo = sizeMo * sizeKb;
        float sizeTerra = sizeGo * sizeKb;


        if (size < sizeMo)
            return df.format(size / sizeKb) + " Kb";
        else if (size < sizeGo)
            return df.format(size / sizeMo) + " Mb";
        else if (size < sizeTerra)
            return df.format(size / sizeGo) + " Gb";

        return "";
    }


    public static void deleteInstanceIdInBackground(final Context mContext, final String token) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
//                try {
////                    InstanceID instanceID = InstanceID.getInstance(mContext);
////
////                    instanceID.deleteInstanceID();
////                    instanceID.deleteToken(token, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
//                } catch (final IOException e) {
//                    Log.d("DeleteIstanceID",e.toString());
//                }
                return null;
            }
        }.execute();
    }

    public static String getTimeAgo(long time) {
        final List<Long> times = Arrays.asList(
                TimeUnit.DAYS.toMillis(365),
                TimeUnit.DAYS.toMillis(30),
                TimeUnit.DAYS.toMillis(1),
                TimeUnit.HOURS.toMillis(1),
                TimeUnit.MINUTES.toMillis(1),
                TimeUnit.SECONDS.toMillis(1));
        final List<String> timesString = Arrays.asList("year", "month", "day", "hour", "minute", "second");
        final int SECOND_MILLIS = 1000;
        final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        final int DAY_MILLIS = 24 * HOUR_MILLIS;
        final int MONTH_MILLIS = 30 * HOUR_MILLIS;
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = new Date().getTime();
        if (time > now || time <= 0) {
            return "just now";
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "الأن";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "منذ دقيقة";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return "منذ " + diff / MINUTE_MILLIS + "دقيقة";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "منذ ساعة";
        } else if (diff < 24 * HOUR_MILLIS) {
            if (diff / HOUR_MILLIS == 1)
                return "منذ " + "ساعة";
            else if (diff / HOUR_MILLIS == 2)
                return "منذ " + "ساعتين";
            else
                return "منذ " + diff / HOUR_MILLIS + "ساعات";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "بالأمس";
        } else if (diff < 30 * DAY_MILLIS) {
            if (diff / DAY_MILLIS < 10)
                return "منذ " + diff / DAY_MILLIS + " أيام";
            else
                return "منذ " + diff / DAY_MILLIS + " يوم";
        } else {
            if (diff / MONTH_MILLIS == 1)
                return "منذ " + diff / MONTH_MILLIS + " شهر ";
            else
                return "منذ " + diff / MONTH_MILLIS + " أشهر ";
        }
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static Drawable GetVectorWithColor(Activity activity, String Color, int Drawable) {
        final android.graphics.drawable.Drawable drawable = DrawableHelper
                .withContext(activity)
                .withColor(Color)
                .withDrawable(Drawable)
                .tint()
                .get();
        return drawable;
    }

    public static void hideSoftKey(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static Bitmap getScaledBitmap(String picturePath, int width, int height) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, sizeOptions);

        int inSampleSize = calculateInSampleSize(sizeOptions, width, height);

        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;

        return BitmapFactory.decodeFile(picturePath, sizeOptions);
    }

    public static void ShowRegisterPopUp(final Activity context, final Class login) {
        new SweetAlertDialogFailed(context, SweetAlertDialogFailed.WARNING_TYPE)
                .setTitleText("You must sign in first !")
                .setContentText("Do you want to sign in now ?")
                .setCancelText("Yes")
                .setConfirmText(context.getResources().getString(R.string.cancel))
                .setCancelClickListener(new SweetAlertDialogFailed.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialogFailed sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        context.startActivity(new Intent(context, login));
//                ActivitySwping.goTOAnotherActivityAndFinish(context, Activity_OnBoarding.class);

                    }
                }).show();
        ;

    }

    public static void HandleActionBar(Activity activity, ActionBar ab, String title) {

        TextView tv = new TextView(activity);

        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView


        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);
        tv.setX(-20);
        // Set text to display in TextView
        tv.setText(title);
        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        tv.setTypeface(null, Typeface.BOLD);
        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);

    }

    public static void ShowSuccessPopUp(final Activity context, String Title, String Content, SweetAlertDialog.OnSweetClickListener listener) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(Title)
                .setContentText(Content)
                .setConfirmText(context.getResources().getString(R.string.ok))
                .setConfirmClickListener(listener)
                .show();

    }

    public static void ShowErrorPopUp(final Activity context, String Title, String Content) {
        new SweetAlertDialogFailed(context, SweetAlertDialogFailed.WARNING_TYPE)
                .setTitleText(Title)
                .setContentText(Content)
                .setConfirmText(context.getResources().getString(R.string.ok))
                .show();

    }

    public static void NoInternetConnection(final Activity context) {
        new SweetAlertDialogFailed(context, SweetAlertDialogFailed.WARNING_TYPE)
                .setTitleText("Note")
                .setContentText("Check your internet connection")
                .setConfirmText("OK")
                .show();
        ;

    }


    public static Bitmap blurRenderScript(Context context, Bitmap smallBitmap, int radius) {

        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }


    public static Date getDate(long time) {
        try {

            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date netDate = (new Date(time * 1000L));
            return netDate;
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getIMEI(Context context) {
        try {
            TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
            String imei = mngr.getDeviceId();
            return imei;
        } catch (RuntimeException e) {

        }

        return null;
    }

    public static String getMd5Key(String password) {

//        String password = "12131123984335";

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            System.out.println("Digest(in hex format):: " + sb.toString());

            //convert the byte to hex format method 2
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            System.out.println("Digest(in hex format):: " + hexString.toString());

            return hexString.toString();

        } catch (Exception e) {
            // TODO: handle exception
        }

        return "";
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public static boolean haveNetworkConnection(Activity activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static boolean checkPlayServices(Activity context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, 9000)
                        .show();
            } else {
                Log.i("PLAYSERVICES", "This device is not supported.");
                context.finish();
            }
            return false;
        }
        return true;
    }

    public static Date convertStringToDate(String str_date) {
        DateFormat formatter;
        try {

            formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = (Date) formatter.parse(str_date);

            return Utils.removeTime(date);
        } catch (ParseException e) {
            formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = null;
            try {
                date = (Date) formatter.parse(str_date);
                return Utils.removeTime(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }


        }
        return Utils.removeTime(new Date());
    }

    public static String convertDateToString(Date str_date) {
        DateFormat formatter;
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(str_date);
    }

    public static long convertStringToTimestamp(String str_date) {
        try {
            DateFormat formatter;

            formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = (Date) formatter.parse(str_date);
            // Log.d("convertStringToTimestamp",date.getTime()+"--");
            // Timestamp timeStampDate = new Timestamp(date.getTime());
            long Time = date.getTime() / 1000;
            Log.d("testtime", Time + "==");

            return Time + 7200;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return 0;
        }
    }

    public static String convertDoubleToString(Object object) {
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(0);

        try {

            return "0" + df.format(object);
        } catch (IllegalArgumentException e) {
            return object.toString();
        }

    }

    public static long convertStringToTimestampCale(String str_date) {
        try {
            DateFormat formatter;
            Log.d("testtime", str_date + "==");

            formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = (Date) formatter.parse(str_date);
            // Log.d("convertStringToTimestamp",date.getTime()+"--");
            // Timestamp timeStampDate = new Timestamp(date.getTime());
            long Time = date.getTime() / 1000;
            Log.d("testtime", Time + "==");

            return Time + 10000;
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return 0;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static void onClickAway(Context context, View v) {
        //hide soft keyboard
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
