package com.dev.baseapp.others.helpers;

import android.app.Activity;
import android.util.Log;

import com.dev.baseapp.others.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;



public class ResultHandler {

    public ResultHandler()
    {

    }

    public Boolean validateHandlerResult(Activity activity, JSONObject jsonObject)
    {

        if (jsonObject.has("success")||jsonObject.has("access_token"))
        {
            Log.d("Accessed First Log",jsonObject.toString());
            try {
                if (jsonObject.has("success") && jsonObject.getBoolean("success") == true) {
                    Log.d("Accessed Status Log",jsonObject.toString());

                    return true;
                } else if (jsonObject.has("access_token") && !jsonObject.getString("access_token").equalsIgnoreCase(""))
                {
                    Log.d("Accessed Token Log",jsonObject.toString());

                    return true;
                }
                else
                {
                    Log.d("Accessed Error Log",jsonObject.toString());

                    if (jsonObject.has("Errors"))
                    Utils.ShowErrorDialog(activity,jsonObject.getJSONArray("Errors").get(0).toString());
                    else
                    Utils.NoInternetConnection(activity);
//                    SweetAlertDialogFailed sweetAlertDialog = new SweetAlertDialogFailed(activity,SweetAlertDialogFailed.WARNING_TYPE);
//                    sweetAlertDialog.setTitle("Error");
//                    sweetAlertDialog.setConfirmText("Dismiss");
//
//                    sweetAlertDialog.setContentText(jsonObject.getJSONArray("Errors").get(0).toString());
//                    sweetAlertDialog.show();
//                    Toast.makeText(activity, jsonObject.getString("EnglishMessage"), Toast.LENGTH_SHORT).show();
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        else
        {
            if (jsonObject.has("categories"))
            {
                return true;
            }
            else if (jsonObject.has("products"))
            {
                return true;
            }
            else if (jsonObject.has("branches"))
            {
                return true;
            }

            else if (jsonObject.has("Message"))
            {
                try {
                    Utils.ShowErrorDialog(activity,jsonObject.getString("Message"));
                } catch (JSONException e) {
                    Utils.NoInternetConnection(activity);
                }

            }
            else if (jsonObject.has("error_description"))
            {
                try {
                    Utils.ShowErrorDialog(activity,jsonObject.getString("error_description"));
                } catch (JSONException e) {
                    Utils.NoInternetConnection(activity);
                }

            }
            else
            Utils.NoInternetConnection(activity);

//            try {
//                Toast.makeText(activity, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();
//                return false;
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }
       return false;
    }
}
