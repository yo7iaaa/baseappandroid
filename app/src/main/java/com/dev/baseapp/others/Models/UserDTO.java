

package com.dev.baseapp.others.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.dev.baseapp.others.utils.Utils;

import java.io.Serializable;



public class UserDTO implements Serializable {
    private static UserDTO ourInstance = new UserDTO();

    public static UserDTO getInstance() {
        return ourInstance;
    }

    private UserDTO() {
    }

    public static void SetUserDTO(UserDTO dto) {
        ourInstance = dto;
    }

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("DisplayName")
    @Expose
    private String displayName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Locked")
    @Expose
    private boolean locked;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("BirthDate")
    @Expose
    private String birthDate;
    @SerializedName("Nationality")
    @Expose
    private String nationality;
    @SerializedName("ResidenceNo")
    @Expose
    private String residenceNo;
    @SerializedName("PassportNo")
    @Expose
    private String passportNo;
    @SerializedName("Facebook")
    @Expose
    private String facebook;
    @SerializedName("WhatsApp")
    @Expose
    private String whatsApp;
    @SerializedName("ratingAverage")
    @Expose
    private double ratingAverage;
    @SerializedName("ratingsCount")
    @Expose
    private int ratingsCount;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("token_type")
    @Expose
    private String token_type;

    @SerializedName("password")
    @Expose
    private String password;


    private String CountryCode;
    private String WhatsAppCode;

    public String getCountryCode() {
        return CountryCode == null ? "966" : CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getWhatsAppCode() {
        return WhatsAppCode == null ? getCountryCode() : WhatsAppCode;
    }


    public void setWhatsAppCode(String whatsAppCode) {
        WhatsAppCode = whatsAppCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneNumberWithoutCode() {
        return phoneNumber.replace(CountryCode, "");
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }


    public String getAddress1() {
        return address1 != null ? address1 : "";
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2 != null ? address2 : "";
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getBirthDate() {
        return birthDate != null ? Utils.convertDateToString(Utils.convertStringToDate(birthDate)) : "";
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality != null ? nationality : "";
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getResidenceNo() {
        return residenceNo != null ? residenceNo : "";
    }

    public void setResidenceNo(String residenceNo) {
        this.residenceNo = residenceNo;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getWhatsApp() {
        return whatsApp != null ? whatsApp : "";
    }

    public String getWhatsAppWithoutCode() {
        return getWhatsApp().replace(getWhatsAppCode(), "");
    }

    public void setWhatsApp(String whatsApp) {
        this.whatsApp = whatsApp;
    }

    public double getRatingAverage() {
        return ratingAverage;
    }

    public void setRatingAverage(double ratingAverage) {
        this.ratingAverage = ratingAverage;
    }

    public int getRatingsCount() {
        return ratingsCount;
    }

    public void setRatingsCount(int ratingsCount) {
        this.ratingsCount = ratingsCount;
    }


}