package com.dev.baseapp.others.utils;

import android.util.Patterns;

/**
 * Created by Android Developer on 2/24/2016.
 */
public class Validators {




    public static boolean isValidMobile(String target) {
        return target != null && Patterns.PHONE.matcher(target).matches();

    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{2,}\\.[0-9]{2,}\\.[0-9]{2,}\\.[0-9]{2,}\\])|(([a-zA-Z\\-0-9]{2,}+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
