package com.dev.baseapp.others.utils;

/**
 * Created by Android Developer on 1/18/2016.
 */

import android.app.Application;
import android.content.Context;


public class Resetapp extends Application {
    private static Resetapp instance;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        instance = this;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }
}