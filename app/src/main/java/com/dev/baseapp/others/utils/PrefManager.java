package com.dev.baseapp.others.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.dev.baseapp.others.Models.UserDTO;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;



public class PrefManager {
    private static final String Languages = "Language";
    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
    }
    public static SharedPreferences getSharedPreferencesLanguage(Context context) {
        return context.getSharedPreferences(Languages, Context.MODE_PRIVATE);
    }
    public static SharedPreferences getSharedPreferencesGUEST(Context context) {
        return context.getSharedPreferences(Constants.PREF_GUEST, Context.MODE_PRIVATE);
    }

    public static boolean isLangArabic(Context context) {
         SharedPreferences settings = getSharedPreferencesLanguage(context);
        if (settings.getString("lang", "ar").equals("ar"))
        {
            return  true;
        }
        return false;
    }
    public static boolean ChangeLanguage(Context context, Boolean isArabic) {
        SharedPreferences.Editor prefsEditor = getSharedPreferencesLanguage(context).edit();
        if (isArabic)
        {
            prefsEditor.putString("lang","ar");
        }
        else
        {
            prefsEditor.putString("lang","en");

        }
        prefsEditor.apply();
        return true;
    }
    public static boolean setIsLoggedIn(Context context, String user, int loggedIn) {
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();
        prefsEditor.putString(Constants.PREF_USER, user);
//        prefsEditor.putString(Constants.PREF_USER_ACTION, useraction);
        prefsEditor.putInt(Constants.PREF_LOGGED_IN, loggedIn);
        prefsEditor.apply();
        return true;
    }

    public static Boolean isGuestDocumentCreated(Context context) {
        String x = getSharedPreferencesGUEST(context).getString(Constants.PREF_GUEST, null);
        Log.d("TESTGUEST", x + " =- = ");
        Log.d("TESTGUEST", x + " =- = " + (x != null));

        return x != null;
    }

    public static String GetGuestDocumentID(Context context) {
        return getSharedPreferencesGUEST(context).getString(Constants.PREF_GUESTID, "");
    }

    public static boolean setGuestUser(Context context, String user, String docid) {
        SharedPreferences.Editor prefsEditor = getSharedPreferencesGUEST(context).edit();
        prefsEditor.putString(Constants.PREF_GUEST, user);
        prefsEditor.putString(Constants.PREF_GUESTID, docid);
//        prefsEditor.putString(Constants.PREF_USER_ACTION, useraction);
        prefsEditor.apply();
        return true;
    }


    public static boolean saveuserdataharedpreference(Context context, String user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();

        prefsEditor.putString(Constants.PREF_USER, user);

        prefsEditor.apply();
        return true;
    }

    public static boolean saveuseractionsharedpreference(Context context, String useraction) {
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();

        prefsEditor.putString(Constants.PREF_USER_ACTION, useraction);

        prefsEditor.apply();
        return true;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }
        return false;
    }

    public static boolean setCountry(Context context, String user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();
        prefsEditor.putString(Constants.PREF_Country, user);
        prefsEditor.apply();
        return true;
    }

    public static String getCountry(Context context) {
        return getSharedPreferences(context).getString(Constants.PREF_Country, "");
    }

    public static int isLoggedIn(Context context) {
        return getSharedPreferences(context).getInt(Constants.PREF_LOGGED_IN, 0);
    }

    public static Boolean isLoggedUser(Context context) {
        return getSharedPreferences(context).getInt(Constants.PREF_LOGGED_IN, 0) == 1;
    }

    public static String getUser(Context context) {
        return getSharedPreferences(context).getString(Constants.PREF_USER, "");
    }

    public static UserDTO getUserParsed(Context context) {
        String user = getSharedPreferences(context).getString(Constants.PREF_USER, null);
        if (user == null)
            return null;
        return new Gson().fromJson(user, UserDTO.class);
    }

    public static JSONObject getUserParsedJSON(Context context) throws JSONException {
        String user = getSharedPreferences(context).getString(Constants.PREF_USER, null);
        if (user == null)
            return null;
        return new JSONObject(user);
    }

    public static JSONObject getUserACTIOnParsedJSON(Context context) throws JSONException {
        String user = getSharedPreferences(context).getString(Constants.PREF_USER_ACTION, null);
        if (user == null)
            return null;
        return new JSONObject(user);
    }

    public static void clear(Context context) {
        getSharedPreferences(context).edit().clear().apply();
    }
}
