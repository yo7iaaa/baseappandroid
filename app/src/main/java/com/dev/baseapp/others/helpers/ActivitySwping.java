package com.dev.baseapp.others.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mahmoud adel on 8/23/2015.
 */
public class ActivitySwping {

    public static void goTOAnotherActivity(Context c, Class<?> clas) {
        Intent i = new Intent(c, clas);
        c.startActivity(i);

    }
    public static void goTOAnotherActivityAndFinish(Activity c, Class<?> clas) {
        Intent i = new Intent(c, clas);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        c.finish();
        c.startActivity(i);

    }
}
